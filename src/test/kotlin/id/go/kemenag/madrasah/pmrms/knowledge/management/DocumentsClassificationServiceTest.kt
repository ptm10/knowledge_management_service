package id.go.kemenag.madrasah.pmrms.knowledge.management

import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.service.DocumentsClassificationService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@Suppress("UNCHECKED_CAST")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class DocumentsClassificationServiceTest {

    @Autowired
    private lateinit var service: DocumentsClassificationService

    @Test
    @DisplayName("Test Datatable")
    fun test1() {
        println("Test Datatable")
        val test: ResponseEntity<ReturnData> = service.datatable(Pagination2Request())
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}
