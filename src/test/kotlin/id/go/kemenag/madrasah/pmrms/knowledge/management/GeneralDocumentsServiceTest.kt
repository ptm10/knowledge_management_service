package id.go.kemenag.madrasah.pmrms.knowledge.management

import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.DocumentClassificationRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.service.GeneralDocumentsService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@Suppress("UNCHECKED_CAST")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class GeneralDocumentsServiceTest {

    @Autowired
    private lateinit var service: GeneralDocumentsService

   /* @Test
    @DisplayName("Test Save")
    fun test1() {
        println("Test Save")
        val test: ResponseEntity<ReturnData> = service.saveData(
            DocumentClassificationRequest(
                name = "test documents",
                filesId = "8a5e0f8c-e9bd-4df1-a18f-98e130d01249",
                documentsClassificationId = "662cf0d2-824d-11ec-8f5f-da6b80a143e9dsadasdas",
                version = "v1",
                description = "description"
            )
        )
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }*/

    @Test
    @DisplayName("Test Datatable")
    fun test2() {
        println("Test Datatable")
        val test: ResponseEntity<ReturnData> =
            service.datatable(Pagination2Request())
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}
