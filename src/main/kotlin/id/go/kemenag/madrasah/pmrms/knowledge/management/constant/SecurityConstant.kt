package id.go.kemenag.madrasah.pmrms.knowledge.management.constant

const val TOKEN_PREFIX = "Bearer "
const val HEADER_STRING = "Authorization"
const val HEADER_KEY_STRING = "x-access-key"
const val TOKEN_STATIC = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJQTVJNUyBIb3N0Mkhvc3QiLCJpYXQiOjE2NjU3MzIyNzEsImV4cCI6MTY5NzI2ODI3MSwiYXVkIjoiZXh0ZXJuYWwiLCJzdWIiOiJUb2tlbiBJbnRlZ3JhdGlvbiIsInByb3ZpZGVyIjoiZXh0ZXJuYWwifQ.1wnFFcOrzhrPKgf_qe4bAnxZrAkTvH4_mKNcv10zkLQ"
val USER_ADMIN_ALLOWED_PATH =
    listOf(
        "/administrative-documents/*",
        "/general-documents/*",
        "/general-documents-version/*",
        "/letter-code/*"
    )

val AUDIENCE_FILTER_PATH = mapOf(
    "user-admin" to USER_ADMIN_ALLOWED_PATH,
    "external" to listOf("/administrative-documents/*", "/letter-code/*")
)
