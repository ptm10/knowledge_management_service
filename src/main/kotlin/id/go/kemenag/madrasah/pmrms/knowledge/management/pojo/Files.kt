package id.go.kemenag.madrasah.pmrms.knowledge.management.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "files", schema = "public")
data class Files(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "filepath")
    var filepath: String? = null,

    @Column(name = "filename")
    var filename: String? = null,

    @Column(name = "file_ext")
    var fileExt: String? = null,

    @Column(name = "attachment_type_id")
    var attachmentTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "attachment_type_id", insertable = false, updatable = false, nullable = true)
    var attachmentType: AttachmentType? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date()
)
