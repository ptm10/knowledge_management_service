package id.go.kemenag.madrasah.pmrms.knowledge.management.model.response

data class DatatableResponse(
    var content: Any? = null,
    var first: Boolean? = null,
    var last: Boolean? = null,
    var totalElements: Long? = null,
    var totalPages: Int? = null
)

data class DatatableResponseRow(
    var content: Any? = null,
    var pageable: Any? = null,
    var totalElements: Long = 0,
    var totalPages: Int = 0,
    var last: Boolean? = null,
    var numberOfElements: Int = 0,
    var size: Int = 0,
    var number: Int = 0,
    var first: Boolean? = null,
    var sort: Any? = null,
    var empty: Boolean? = null
)