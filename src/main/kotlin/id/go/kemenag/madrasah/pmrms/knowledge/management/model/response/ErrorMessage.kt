package id.go.kemenag.madrasah.pmrms.knowledge.management.model.response

data class ErrorMessage(
    var key: String? = null,
    var message: String? = null
)
