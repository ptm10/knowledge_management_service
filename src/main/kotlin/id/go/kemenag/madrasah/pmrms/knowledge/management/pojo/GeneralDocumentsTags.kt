package id.go.kemenag.madrasah.pmrms.knowledge.management.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "general_documents_tags", schema = "public")
data class GeneralDocumentsTags(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "general_documents_id")
    var generalDocumentsId: String? = null,

    @Column(name = "general_documents_tag_id")
    var generalDocumentsTagId: String? = null,

    @ManyToOne
    @JoinColumn(name = "general_documents_tag_id", insertable = false, updatable = false, nullable = true)
    var generalDocumentsTag: GeneralDocumentsTag? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
