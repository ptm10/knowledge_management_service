package id.go.kemenag.madrasah.pmrms.knowledge.management.model.users

import java.util.*

data class Component(
    var id: String? = UUID.randomUUID().toString(),

    var code: String? = null,

    var description: String? = null,
)
