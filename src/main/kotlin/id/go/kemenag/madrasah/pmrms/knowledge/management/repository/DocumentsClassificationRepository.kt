package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.DocumentsClassification
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface DocumentsClassificationRepository : JpaRepository<DocumentsClassification, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<DocumentsClassification>
}
