package id.go.kemenag.madrasah.pmrms.knowledge.management.service

import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native.GeneralDocumentTagRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Suppress("UNCHECKED_CAST")
@Service
class GeneralDocumentsTagService {

    @Autowired
    private lateinit var repoNative: GeneralDocumentTagRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
