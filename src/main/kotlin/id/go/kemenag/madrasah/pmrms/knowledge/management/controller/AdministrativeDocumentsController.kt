package id.go.kemenag.madrasah.pmrms.knowledge.management.controller

import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.AdministrativeDocumentRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.AktivationLetterNumbersRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.CancellationLetterNumbersRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.service.AdministrativeDocumentsService
import io.swagger.annotations.Api
import javax.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Administrative Documents"], description = "Administrative Documents API")
@RestController
@RequestMapping(path = ["administrative-documents"])
class AdministrativeDocumentsController {

    @Autowired private lateinit var service: AdministrativeDocumentsService

    @PostMapping(value = [""], produces = ["application/json"])
    fun saveData(
            @Valid @RequestBody request: AdministrativeDocumentRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PostMapping(value = ["cancellation"], produces = ["application/json"])
    fun saveCancellation(
        @Valid @RequestBody request: CancellationLetterNumbersRequest
    ): ResponseEntity<ReturnData> {
        return service.saveCancellation(request)
    }

    @PostMapping(value = ["activation"], produces = ["application/json"])
    fun saveActivation(
        @Valid @RequestBody request: AktivationLetterNumbersRequest
    ): ResponseEntity<ReturnData> {
        return service.saveActivation(request)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

}
