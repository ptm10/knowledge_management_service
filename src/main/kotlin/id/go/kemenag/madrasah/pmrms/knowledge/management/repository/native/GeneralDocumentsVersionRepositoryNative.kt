package id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.GeneralDocumentsVersion
import org.springframework.stereotype.Repository

@Repository
class GeneralDocumentsVersionRepositoryNative :
    BaseRepositoryNative<GeneralDocumentsVersion>(GeneralDocumentsVersion::class.java)
