package id.go.kemenag.madrasah.pmrms.knowledge.management.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "letter_code", schema = "master")
data class LetterCode (
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "code")
    var code: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)

data class StaticLetterCode (
    var code: String? = "",

    var name: String? = "",

    var description: String? = "",
)