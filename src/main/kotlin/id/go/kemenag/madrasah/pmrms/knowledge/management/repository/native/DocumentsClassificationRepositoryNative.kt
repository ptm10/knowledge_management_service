package id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.DocumentsClassification
import org.springframework.stereotype.Repository

@Repository
class DocumentsClassificationRepositoryNative :
    BaseRepositoryNative<DocumentsClassification>(DocumentsClassification::class.java)
