package id.go.kemenag.madrasah.pmrms.knowledge.management.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.users.Users
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "general_documents_version", schema = "public")
data class GeneralDocumentsVersion(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "general_documents_id")
    var generalDocumentsId: String? = null,

    @Column(name = "files_id")
    var filesId: String? = null,

    @ManyToOne
    @JoinColumn(name = "files_id", insertable = false, updatable = false, nullable = true)
    var files: Files? = null,

    @Column(name = "version")
    var version: String? = null,

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUsers: Users? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
