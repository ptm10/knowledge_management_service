package id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.AttachmentType
import org.springframework.stereotype.Repository

@Repository
class AttachmentTypeRepositoryNative : BaseRepositoryNative<AttachmentType>(AttachmentType::class.java)
