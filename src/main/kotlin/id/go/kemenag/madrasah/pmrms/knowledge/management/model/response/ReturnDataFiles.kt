package id.go.kemenag.madrasah.pmrms.knowledge.management.model.response

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.Files

data class ReturnDataFiles(
    var success: Boolean? = false,
    var data: Files? = null,
    var message: String? = null
)
