package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.GeneralDocumentsVersion
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface GeneralDocumentsVersionRepository : JpaRepository<GeneralDocumentsVersion, String> {

    @Query("FROM GeneralDocumentsVersion WHERE generalDocumentsId = :generalDocumentsId AND LOWER(TRIM(version)) = LOWER(TRIM(:version)) AND active = true")
    fun findByGeneralDocumentsIdVersion(
        generalDocumentsId: String?,
        version: String?
    ): Optional<GeneralDocumentsVersion>
}
