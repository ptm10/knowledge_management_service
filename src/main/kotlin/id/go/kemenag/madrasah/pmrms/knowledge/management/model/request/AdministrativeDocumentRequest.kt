package id.go.kemenag.madrasah.pmrms.knowledge.management.model.request

import id.go.kemenag.madrasah.pmrms.knowledge.management.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class AdministrativeDocumentRequest(
        // @field:NotEmpty(message = "Nama $VALIDATOR_MSG_REQUIRED") var name: String? = null,
        // @field:NotEmpty(message = "Id File $VALIDATOR_MSG_REQUIRED") var filesId: String? = null,
        // @field:NotEmpty(message = "Versi $VALIDATOR_MSG_REQUIRED") var version: String? = null,
        
        // var meetingDate: String? = null,

        var documentProperties: String? = null, // B: Biasa, R: Rahasia, SR: Sangat Rahasia
        var workUnitId: String? = null,
        var jobCode: String? = "KT",
        var archiveCode: String? = null, 
        var componentId: String? = null,
        var letterCode: String? = null,
        var letterNumberDate: String? = null,
        var total: Int = 1,
        var description: String? = null
)

data class CancellationLetterNumbersRequest(
        var letterNumber: String? = null,
        var note: String? = null
)

data class AktivationLetterNumbersRequest(
        var letterNumber: String? = null,
        var note: String? = null
)