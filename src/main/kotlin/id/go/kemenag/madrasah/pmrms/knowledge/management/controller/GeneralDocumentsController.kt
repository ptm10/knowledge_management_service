package id.go.kemenag.madrasah.pmrms.knowledge.management.controller

import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.DocumentClassificationRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.service.GeneralDocumentsService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Api(tags = ["General Documents"], description = "General Documents API")
@RestController
@RequestMapping(path = ["general-documents"])
class GeneralDocumentsController {

    @Autowired
    private lateinit var service: GeneralDocumentsService

    @PostMapping(value = [""], produces = ["application/json"])
    fun saveData(
        @Valid @RequestBody request: DocumentClassificationRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

}
