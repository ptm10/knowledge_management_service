package id.go.kemenag.madrasah.pmrms.knowledge.management.service

import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.PaginationRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.LetterCode
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.StaticLetterCode
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.LetterCodeRepository
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native.LetterCodeRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Suppress("UNCHECKED_CAST")
@Service
class LetterCodeService {

    @Autowired
    private lateinit var repo: LetterCodeRepository

    @Autowired
    private lateinit var repoNative: LetterCodeRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun letterCode(page: PaginationRequest): ResponseEntity<ReturnData> {
        return try {
            val page: Page<LetterCode?>? = if ((page.enablePage ?: 1) == 1) {
                var direction: Sort.Direction = Sort.Direction.ASC
                if (page.sort?.lowercase(Locale.getDefault()).equals("desc")) {
                    direction = Sort.Direction.DESC
                }
                repo.getListPagination(
                    param = page.param.toString().lowercase(Locale.getDefault()),
                    pageable = PageRequest.of(page.page ?: 0, page.size ?: 10, Sort.by(direction, page.sortBy))
                )
            } else {
                repo.getListPagination(
                    param = page.param.toString().lowercase(Locale.getDefault()),
                    pageable = Pageable.unpaged()
                )
            }
            responseSuccess(data = page)
        } catch (e: Exception) {
            throw e
        }
    }

    //sifat surat
    fun docProperties(page: PaginationRequest): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = listOf(
                StaticLetterCode("B", "Biasa", ""),
                StaticLetterCode("R", "Rahasia", ""),
                StaticLetterCode("SR", "Sangat Rahasia", "")
            ))
        } catch (e: Exception) {
            throw e
        }
    }

    //unit kerja
    fun workUnit(page: PaginationRequest): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = listOf(
                StaticLetterCode("PMU.MEQR", "PMU.MEQR", ""),
                StaticLetterCode("PCU.MEQR", "PCU.MEQR", ""),
                StaticLetterCode("DCU.MEQR", "DCU.MEQR", "")
            ))
        } catch (e: Exception) {
            throw e
        }
    }
}
