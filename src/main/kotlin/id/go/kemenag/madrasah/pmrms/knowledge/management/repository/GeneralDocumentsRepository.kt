package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.GeneralDocuments
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface GeneralDocumentsRepository : JpaRepository<GeneralDocuments, String> {
    @Query("FROM GeneralDocuments WHERE LOWER(TRIM(name)) = LOWER(TRIM(:name)) AND componentId = :componentId AND active = true")
    fun findByNameComponent(
        name: String?,
        componentId: String?
    ): Optional<GeneralDocuments>
}
