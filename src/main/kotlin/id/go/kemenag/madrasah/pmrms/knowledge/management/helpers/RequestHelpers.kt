package id.go.kemenag.madrasah.pmrms.knowledge.management.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.knowledge.management.constant.HEADER_STRING
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnDataFiles
import kong.unirest.Unirest


class RequestHelpers {

    companion object {

        fun authDetail(baseUrl: String, bearer: String): ReturnData? {
            return try {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$baseUrl/auth/detail"
                val response = Unirest.get(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .asString()

                objectMapper.readValue(response.body, ReturnData::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        fun filesGetById(id: String, baseUrl: String, bearer: String): ReturnDataFiles? {
            return try {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$baseUrl/files/get-by-id?id=$id"
                val response = Unirest.get(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .asString()

                objectMapper.readValue(response.body, ReturnDataFiles::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }
}
