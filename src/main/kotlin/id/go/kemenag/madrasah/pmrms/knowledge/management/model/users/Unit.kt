package id.go.kemenag.madrasah.pmrms.knowledge.management.model.users

data class Unit(

    var id: String? = null,

    var name: String? = null
)
