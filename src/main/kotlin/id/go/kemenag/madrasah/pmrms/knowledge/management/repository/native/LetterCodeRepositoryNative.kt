package id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.LetterCode
import org.springframework.stereotype.Repository

@Repository
class LetterCodeRepositoryNative : BaseRepositoryNative<LetterCode>(LetterCode::class.java)
