package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.AdministrativeDocuments
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface AdministrativeDocumentsRepository : JpaRepository<AdministrativeDocuments, String> {

    fun findByLetterNumber(letter_number: String?): Optional<AdministrativeDocuments>

    @Query("FROM AdministrativeDocuments WHERE LOWER(TRIM(name)) = LOWER(TRIM(:name)) AND componentId = :componentId AND active = true")
    fun findByNameComponent(
        name: String?,
        componentId: String?
    ): Optional<AdministrativeDocuments>

    @Query("select ad.id from AdministrativeDocuments ad")
    fun getCountAdmDoc(): List<String>
}
