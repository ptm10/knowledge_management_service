package id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.GeneralDocumentsTag
import org.springframework.stereotype.Repository

@Repository
class GeneralDocumentTagRepositoryNative : BaseRepositoryNative<GeneralDocumentsTag>(GeneralDocumentsTag::class.java)
