package id.go.kemenag.madrasah.pmrms.knowledge.management.service

import id.go.kemenag.madrasah.pmrms.knowledge.management.constant.DOCUMENTS_TYPE_GENERAL
import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native.AttachmentTypeRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class DocumentsTypeService {

    @Autowired
    private lateinit var repo: AttachmentTypeRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            req.paramIn?.add(ParamArray("fileType", "string", DOCUMENTS_TYPE_GENERAL))
            responseSuccess(data = repo.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
