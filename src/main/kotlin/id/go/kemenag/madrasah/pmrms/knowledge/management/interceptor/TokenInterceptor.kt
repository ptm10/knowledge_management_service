package id.go.kemenag.madrasah.pmrms.knowledge.management.interceptor

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.users.Users
import id.go.kemenag.madrasah.pmrms.knowledge.management.constant.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.RequestHelpers
import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.getEnvFromHttpServletRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.util.ContentCachingRequestWrapper
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TokenInterceptor : Filter {

    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        val req = ContentCachingRequestWrapper(request as HttpServletRequest)
        val res = response as HttpServletResponse

        // Read the Authorization header, where the JWT token should be
        val header = req.getHeader(HEADER_STRING)
        val headerKey = req.getHeader(HEADER_KEY_STRING)

        // If header doesn't contain Bearer or is null will return nothing and exit
        if (header == null && headerKey == null
            //|| !header.startsWith(TOKEN_PREFIX)
         ) {
            responseException(res, "Token $VALIDATOR_MSG_REQUIRED")
            return
        }

        var authentication: UsernamePasswordAuthenticationToken? = null
        var successAuth = true

        try {
            authentication = if (header != null && header.startsWith(TOKEN_PREFIX))
                getAuthentication(req)
            else
                validateToken(req)

        } catch (e: Exception) {
            println("responseException")
            successAuth = false
            responseException(res, e.message.toString())
        }

        SecurityContextHolder.getContext().authentication = authentication
        if (successAuth) {
            chain?.doFilter(request, response)
        }
    }

    private fun responseException(response: HttpServletResponse, message: String) {
        val json = ObjectMapper().writeValueAsString(
            ReturnData(
                message = message
            )
        )

        response.contentType = "application/json"
        response.status = HttpServletResponse.SC_UNAUTHORIZED
        response.writer.write(json)
        response.writer.flush()
        response.writer.close()
    }

    private fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken {
        try {
            val token = request.getHeader(HEADER_STRING)
            val reqAuthDetail: ReturnData =
                RequestHelpers.authDetail(getEnvFromHttpServletRequest(request, "url.auth"), token)
                    ?: throw Exception("User $VALIDATOR_MSG_NOT_FOUND")

            reqAuthDetail.success?.let {
                if (!it) {
                    throw Exception(reqAuthDetail.message)
                }
            }

            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            val users: Users =
                objectMapper.readValue(objectMapper.writeValueAsString(reqAuthDetail.data), Users::class.java)

            val audience = "user-admin"

            var isAllowed = false

            AUDIENCE_FILTER_PATH[audience]?.forEach lambda@{
                if (it.contains("/*")) {
                    if (request.requestURI.contains(it.replace("/*", ""))) {
                        isAllowed = true
                        return@lambda
                    }
                } else {
                    if (request.requestURI == it) {
                        isAllowed = true
                        return@lambda
                    }
                }
            }

            return if (isAllowed) {
                UsernamePasswordAuthenticationToken(
                    ObjectMapper().writeValueAsString(users),
                    audience,
                    arrayListOf()
                )
            } else {
                throw Exception("User $VALIDATOR_MSG_NOT_HAVE_ACCESS")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }


    private fun validateToken(request: HttpServletRequest): UsernamePasswordAuthenticationToken {
        try {
            val token = request.getHeader(HEADER_KEY_STRING)
            if (token != TOKEN_STATIC)
                throw Exception("User Client $VALIDATOR_MSG_NOT_HAVE_ACCESS")
//            val reqAuthDetail: ReturnData =
//                RequestHelpers.authDetail(getEnvFromHttpServletRequest(request, "url.auth"), token)
//                    ?: throw Exception("User $VALIDATOR_MSG_NOT_FOUND")
//
//            reqAuthDetail.success?.let {
//                if (!it) {
//                    throw Exception(reqAuthDetail.message)
//                }
//            }

            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
//            val users: Users =
//                objectMapper.readValue(objectMapper.writeValueAsString(reqAuthDetail.data), Users::class.java)

            val audience = "external"
            var isAllowed = false

            AUDIENCE_FILTER_PATH[audience]?.forEach lambda@{
                if (it.contains("/*")) {
                    if (request.requestURI.contains(it.replace("/*", ""))) {
                        isAllowed = true
                        return@lambda
                    }
                } else {
                    if (request.requestURI == it) {
                        isAllowed = true
                        return@lambda
                    }
                }
            }

            return if (isAllowed) {
                UsernamePasswordAuthenticationToken(
                    null, //ObjectMapper().writeValueAsString(users),
                    audience,
                    arrayListOf()
                )
            } else {
                throw Exception("User $VALIDATOR_MSG_NOT_HAVE_ACCESS")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }

}
