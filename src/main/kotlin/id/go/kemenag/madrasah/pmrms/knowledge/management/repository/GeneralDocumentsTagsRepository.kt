package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.GeneralDocumentsTags
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface GeneralDocumentsTagsRepository : JpaRepository<GeneralDocumentsTags, String> {

    @Query("select gd.generalDocumentsId from GeneralDocumentsTags gd where gd.active = true and gd.generalDocumentsTagId in(:tags)")
    fun getDocumentIdsByTags(tags: List<String>): List<String>
}
