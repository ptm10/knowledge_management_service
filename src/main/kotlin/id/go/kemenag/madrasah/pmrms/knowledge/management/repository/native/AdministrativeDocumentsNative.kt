package id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.AdministrativeDocuments
import org.springframework.stereotype.Repository

@Repository
class AdministrativeDocumentsNative : BaseRepositoryNative<AdministrativeDocuments>(AdministrativeDocuments::class.java)
