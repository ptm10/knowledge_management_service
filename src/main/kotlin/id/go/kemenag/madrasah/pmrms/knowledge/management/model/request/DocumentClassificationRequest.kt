package id.go.kemenag.madrasah.pmrms.knowledge.management.model.request

import id.go.kemenag.madrasah.pmrms.knowledge.management.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class DocumentClassificationRequest(

    @field:NotEmpty(message = "Nama $VALIDATOR_MSG_REQUIRED")
    var name: String? = null,

    @field:NotEmpty(message = "Id File $VALIDATOR_MSG_REQUIRED")
    var filesId: String? = null,

    @field:NotEmpty(message = "Versi $VALIDATOR_MSG_REQUIRED")
    var version: String? = null,

    var componentId: String? = null,

    var meetingDate: String? = null,

    var description: String? = null,

    var tagIds: List<String>? = null,

    var newTags: List<String>? = null
)
