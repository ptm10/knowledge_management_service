package id.go.kemenag.madrasah.pmrms.knowledge.management.service

import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native.GeneralDocumentsVersionRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class GeneralDocumentsVersionService {

    @Autowired
    private lateinit var repo: GeneralDocumentsVersionRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repo.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
