package id.go.kemenag.madrasah.pmrms.knowledge.management.service
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.knowledge.management.constant.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.AdministrativeDocumentRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.AktivationLetterNumbersRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.CancellationLetterNumbersRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.DatatableResponse
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.DatatableResponseRow
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.users.Users
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native.AdministrativeDocumentsNative
import kong.unirest.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST")
@Service
class AdministrativeDocumentsService {

    @Autowired private lateinit var repo: AdministrativeDocumentsRepository

    @Autowired private lateinit var repoNative: AdministrativeDocumentsNative

    @Autowired private lateinit var repoComponent: ComponentRepository

    @Autowired private lateinit var httpServletRequest: HttpServletRequest

    

    fun saveData(request: AdministrativeDocumentRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validate(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }


            val letterNumbers = ArrayList<String>()
            val details = mutableListOf<AdministrativeDocuments>()
            var count = repo.getCountAdmDoc().count()
            for (i in 0 until request.total) {
                //"${intToRoman((validate["letterNumberDate"] as Date).month + 1)}/" +
                var letterNumber =
                    "${request.documentProperties}-" +
                    "${validate["letterNumberDay"]}.${count++}/" +
                    "${request.jobCode}-${request.workUnitId}/" +
                    "${request.letterCode}/" +
                    "${validate["letterNumberMonth"]}/${validate["letterNumberYear"]}"
                details.add(
                    AdministrativeDocuments(
                        documentProperties= request.documentProperties,
                        workUnitId = request.workUnitId,
                        jobCode= request.jobCode,
                        componentId = validate["componentId"].toString(),
                        letterCode = request.letterCode,
                        letterNumberDate = validate["letterNumberDate"] as Date,
                        status = "Digunakan",
                        description = request.description,
                        createdBy = getUserLogin()?.id,
                        letterNumber = letterNumber
                    )
                )
                letterNumbers.add(letterNumber!!)
            }

            repo.saveAll(details)
                return responseCreated(data = letterNumbers)
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
           if (userHasRoles(listOf(ROLE_ID_SEK))) {
            req.paramIs?.add(ParamSearch("createdBy", "string", getUserLogin()?.id))
           }
            val content = ArrayList<Any>()
            val _data = repoNative.getPage(req)
            _data?.content?.forEachIndexed { index, element ->
                val _dta = element as AdministrativeDocuments
                content.add(AdministrativeDocumentsResponse(
                    rowNumber = index + 1,
                    id = _dta?.id,
                    documentProperties= _dta.documentProperties,
                    workUnitId= _dta?.workUnitId,
                    jobCode= _dta.jobCode,
                    component= _dta?.component,
                    componentId= _dta?.componentId,
                    letterCode= _dta?.letterCode,
                    letterNumberDate= _dta?.letterNumberDate,
                    letterNumber= _dta?.letterNumber,
                    description= _dta?.description,
                    status= _dta?.status,
                    note= _dta?.note,
                    createdAt= _dta?.createdAt,
                    createdBy= _dta?.createdBy,
                    createdByUsers= _dta?.createdByUsers,
                    updatedAt= _dta?.updatedAt,
                    active= _dta?.active
                ) )
            }

            responseSuccess(data = DatatableResponseRow(
                content = content,
                pageable =  _data?.pageable,

                totalElements=  _data!!.totalElements,
                totalPages =  _data!!.totalPages,
                last=  _data?.isLast,
                numberOfElements=  _data?.numberOfElements,
                size=  _data?.size,
                number=  _data?.number,
                first=  _data?.isFirst,
                sort=  _data?.sort,
                empty=  _data?.isEmpty)
            )
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveCancellation(request: CancellationLetterNumbersRequest): ResponseEntity<ReturnData> {
        try {
//            val validate = validateUpdateData(request)
//            val listMessage = validate["listMessage"] as List<ErrorMessage>
//            if (listMessage.isNotEmpty()) {
//                return responseBadRequest(data = listMessage)
//            }
            val findOne = repo.findByLetterNumber(request.letterNumber)
            if (!findOne.isPresent) {
                return responseNotFound(message = "Nomor Surat tidak di temukan")
            }
            val update = findOne.get()
            update.status = "Dibatalkan"
            update.note = request.note
            update.updatedBy = getUserLogin()?.id
            repo.save(update)
            return responseSuccess(data = request)
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveActivation(request: AktivationLetterNumbersRequest): ResponseEntity<ReturnData> {
        try {
//            val validate = validateUpdateData(request)
//            val listMessage = validate["listMessage"] as List<ErrorMessage>
//            if (listMessage.isNotEmpty()) {
//                return responseBadRequest(data = listMessage)
//            }
            val findOne = repo.findByLetterNumber(request.letterNumber)
            if (!findOne.isPresent) {
                return responseNotFound(message = "Nomor Surat tidak di temukan")
            }
            val update = findOne.get()
            update.status = "Digunakan"
            update.note = request.note
            update.updatedBy = getUserLogin()?.id
            repo.save(update)
            return responseSuccess(data = request)
        } catch (e: Exception) {
            throw e
        }
    }

    fun intToRoman(number: Int): String? {
        var number = number
        println("number = $number")
        val s = StringBuilder()
        while (number >= 40) {
            s.append("XL")
            number -= 40
        }
        while (number >= 10) {
            s.append("X")
            number -= 10
        }
        while (number >= 9) {
            s.append("IX")
            number -= 9
        }
        while (number >= 5) {
            s.append("V")
            number -= 5
        }
        while (number >= 4) {
            s.append("IV")
            number -= 4
        }
        while (number >= 1) {
            s.append("I")
            number -= 1
        }
        return s.toString()
    }

    private fun validate(request: AdministrativeDocumentRequest): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            // if (!request.filesId.isNullOrEmpty()) {
            //     val chekFile: ReturnDataFiles? =
            //             RequestHelpers.filesGetById(
            //                     request.filesId ?: "",
            //                     filesUrl,
            //                     httpServletRequest.getHeader(HEADER_STRING)
            //             )
            //     val errorMessageFile = "Gagal melakukan request file"
            //     if (chekFile == null) {
            //         listMessage.add(ErrorMessage("filesId", errorMessageFile))
            //     } else {
            //         if (chekFile.success != true) {
            //             listMessage.add(ErrorMessage("filesId", errorMessageFile))
            //         } else {
            //             if (chekFile.data == null) {
            //                 listMessage.add(ErrorMessage("filesId", errorMessageFile))
            //             } else {
            //                 if (chekFile.data?.attachmentTypeId == ATTACHMENT_TYPE_ID_MOM) {
            //                     if (request.meetingDate.isNullOrEmpty()) {
            //                         listMessage.add(
            //                                 ErrorMessage(
            //                                         "meetingDate",
            //                                         "Tanggal Rapat $VALIDATOR_MSG_REQUIRED"
            //                                 )
            //                         )
            //                     } else {
                                    var letterNumberDate: Date? = null
                                    var letterNumberDay: String? = null
                                    var letterNumberMonth: String? = null
                                    var letterNumberYear: String? = null
                                    val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

                                    try {
                                        letterNumberDate = dform.parse(request.letterNumberDate)

                                        // displaying date
                                        var f: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                                        val strDate: String = f.format(letterNumberDate)
                                        println("letterNumber Date = $strDate")

                                        // displaying day
                                        f = SimpleDateFormat("dd")
                                        val strDay: String = f.format(letterNumberDate)
                                        letterNumberDay = strDay
                                        println("letterNumber Day = $strDay")

                                        // displaying month
                                        f = SimpleDateFormat("MM")
                                        val strMonth: String = f.format(letterNumberDate)
                                        letterNumberMonth = strMonth
                                        println("letterNumber Month = $strMonth")

                                        // displaying year
                                        f = SimpleDateFormat("yyyy")
                                        val strYear: String = f.format(letterNumberDate)
                                        letterNumberYear = strYear
                                        println("letterNumber Year = $strYear")
                                    } catch (_: Exception) {
                                        listMessage.add(
                                            ErrorMessage("letterNumberDate","Tanggal No. Surat $VALIDATOR_MSG_NOT_VALID")
                                        )
                                    }

                                    if (letterNumberDate != null) {
                                        rData["letterNumberDate"] = letterNumberDate
                                        rData["letterNumberDay"] = letterNumberDay
                                        rData["letterNumberMonth"] = letterNumberMonth
                                        rData["letterNumberYear"] = letterNumberYear
                                    }
            //                     }
            //                 }

            //                 rData["files"] = chekFile.data!!
            //             }
            //         }
            //     }
            // }

            var componentId = request.componentId
            if (componentId.isNullOrEmpty()) {
                componentId = COMPONENT_EMPTY_ID
                rData["componentId"] = componentId
            }

            // var createdNewVersion: GeneralDocumentsVersion? = null

            val checkComponent = repoComponent.findByIdAndActive(componentId)
            if (!checkComponent.isPresent) {
//                listMessage.add(
//                        ErrorMessage(
//                                "componentId",
//                                "Unit Kerja ${componentId} $VALIDATOR_MSG_NOT_FOUND"
//                        )
//                )
            } else {
                // val check = repo.findByNameComponent(request.name, componentId)

                // if (check.isPresent) {
                //     if (check.get().version?.trim()?.lowercase() ==
                //                     request.version?.trim()?.lowercase()
                //     ) {
                //         listMessage.add(
                //                 ErrorMessage(
                //                         "filesId",
                //                         "Nama File ${request.name}, Versi ${request.version}, Unit Kerja ${checkComponent.get().code} $VALIDATOR_MSG_HAS_ADDED"
                //                 )
                //         )
                //     } else {
                //         val checkVersion =
                //                 repoGeneralDocumentsVersion.findByGeneralDocumentsIdVersion(
                //                         check.get().id,
                //                         request.version
                //                 )

                //         if (checkVersion.isPresent) {
                //             listMessage.add(
                //                     ErrorMessage(
                //                             "filesId",
                //                             "Nama File ${request.name}, Versi ${request.version}, Unit Kerja ${checkComponent.get().code} $VALIDATOR_MSG_HAS_ADDED"
                //                     )
                //             )
                //         } else {
                //             createdNewVersion =
                //                     repoGeneralDocumentsVersion.save(
                //                             GeneralDocumentsVersion(
                //                                     generalDocumentsId = check.get().id,
                //                                     filesId = request.filesId,
                //                                     version = request.version?.trim(),
                //                                     createdBy = getUserLogin()?.id
                //                             )
                //                     )
                //         }
                //     }
                // }
            }

            // val generalDocumentsTags = mutableListOf<GeneralDocumentsTag>()
            // request.tagIds?.forEach {
            //     val checkTagById = repoGeneralDocumentsTag.findByIdAndActive(it)
            //     if (!checkTagById.isPresent) {
            //         listMessage.add(ErrorMessage("tagIds", "Tag Id $it $VALIDATOR_MSG_NOT_FOUND"))
            //     } else {
            //         generalDocumentsTags.add(checkTagById.get())
            //     }
            // }

            // request.newTags?.forEach {
            //     val checkNewTag = repoGeneralDocumentsTag.findByNameActive(it)
            //     if (checkNewTag.isPresent) {
            //         generalDocumentsTags.add(checkNewTag.get())
            //     } else {
            //         generalDocumentsTags.add(
            //                 repoGeneralDocumentsTag.save(GeneralDocumentsTag(name = it.trim()))
            //         )
            //     }
            // }

            // rData["generalDocumentsTags"] = generalDocumentsTags
            // rData["createdNewVersion"] = createdNewVersion

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }
}
