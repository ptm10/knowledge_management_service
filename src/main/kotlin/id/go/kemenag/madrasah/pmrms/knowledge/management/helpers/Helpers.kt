@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package id.go.kemenag.madrasah.pmrms.knowledge.management.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.users.Users
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.users.UsersRole
import org.springframework.core.env.Environment
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.support.WebApplicationContextUtils
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import javax.servlet.http.HttpServletRequest


fun getUserLogin(): Users? {
    return try {
        val principal = SecurityContextHolder.getContext().authentication.principal as Any
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.readValue(principal.toString(), Users::class.java)
    } catch (e: Exception) {
        null
    }
}

@Throws(MalformedURLException::class)
fun getBaseUrl(request: HttpServletRequest): String {
    try {
        val requestURL = URL(request.requestURL.toString())
        val port = if (requestURL.port == -1) "" else ":" + requestURL.port
        return requestURL.protocol + "://" + requestURL.host + port + request.contextPath + "/"
    } catch (e: Exception) {
        throw e
    }
}


fun getUserRoles(): MutableSet<UsersRole> {
    var roles: MutableSet<UsersRole> = mutableSetOf()
    try {
        getUserLogin()?.let {
            if (!it.roles.isNullOrEmpty()) {
                roles = getUserLogin()!!.roles!!
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return roles
}


fun userHasRoles(roles: List<String>): Boolean {
    var allowed = false
    try {
        roles.forEach {
            val findRole: UsersRole? = getUserRoles().find { f ->
                f.roleId == it
            }
            if (findRole != null) {
                allowed = true
                return@forEach
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return allowed
}

fun userHasResources(): Boolean {
    var resources = false
    try {
        resources = getUserLogin()?.resources != null
    } catch (_: Exception) {

    }

    return resources
}


fun getFullUrl(request: HttpServletRequest): String {
    val requestURL = StringBuilder(request.requestURL.toString())
    val queryString = request.queryString
    return if (queryString == null) {
        requestURL.toString()
    } else {
        requestURL.append('?').append(queryString).toString()
    }
}

fun getUserComponetCodes(): List<String> {
    val rData: MutableList<String> = emptyList<String>().toMutableList()
    try {
        val splitCode = "C"
        val user = getUserLogin()
        user?.roles?.forEach {
            val code = it.role?.code ?: ""
            if (code.contains(splitCode)) {
                val subStr = code.substringAfterLast(splitCode)
                if (subStr != "") {
                    rData.add(subStr)
                }
            }
        }

        if (rData.isEmpty()) {
            rData.add("0")
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return rData
}

fun getEnvFromHttpServletRequest(httpServletRequest: HttpServletRequest, key: String): String {
    var rData = ""
    try {
        val servletContext = httpServletRequest.session.servletContext
        val webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        val env: Environment = webApplicationContext!!.getBean(Environment::class.java)
        rData = env.getProperty(key, "")
        println(rData)
    } catch (_: Exception) {

    }
    return rData
}

@Throws(IOException::class)
fun convertMultiPartToFile(file: MultipartFile): File {
    val convFile = File(file.originalFilename)
    val fos = FileOutputStream(convFile)
    fos.write(file.bytes)
    fos.close()
    return convFile
}
