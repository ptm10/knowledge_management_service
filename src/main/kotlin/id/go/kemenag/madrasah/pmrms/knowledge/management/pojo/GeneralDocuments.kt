package id.go.kemenag.madrasah.pmrms.knowledge.management.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.users.Component
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.users.Users
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "general_documents", schema = "public")
data class GeneralDocuments(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "files_id")
    var filesId: String? = null,

    @ManyToOne
    @JoinColumn(name = "files_id", insertable = false, updatable = false, nullable = true)
    var files: Files? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "component_id", insertable = false, updatable = false, nullable = true)
    var component: Component? = null,

    @Column(name = "version")
    var version: String? = null,

    @Column(name = "meeting_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var meetingDate: Date? = null,

    @Column(name = "description")
    var description: String? = null,

    @OneToMany(mappedBy = "generalDocumentsId")
    @Where(clause = "active = true")
    @OrderBy("createdAt")
    var tags: MutableList<GeneralDocumentsTags> = emptyList<GeneralDocumentsTags>().toMutableList(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUsers: Users? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
