package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.GeneralDocumentsTag
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface GeneralDocumentsTagRepository : JpaRepository<GeneralDocumentsTag, String> {

    @Query("FROM GeneralDocumentsTag WHERE LOWER(TRIM(:name)) = LOWER(TRIM(name)) AND active = :active")
    fun findByNameActive(
        @Param("name") name: String?,
        @Param("active") active: Boolean? = true
    ): Optional<GeneralDocumentsTag>

    fun findByIdAndActive(id: String? = null, active: Boolean? = true): Optional<GeneralDocumentsTag>
}
