package id.go.kemenag.madrasah.pmrms.knowledge.management.controller

import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.PaginationRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.service.GeneralDocumentsTagService
import id.go.kemenag.madrasah.pmrms.knowledge.management.service.LetterCodeService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Letter Code"], description = "Letter Code API")
@RestController
@RequestMapping(path = ["letter-code"])
class LetterCodeController {
    @Autowired
    private lateinit var service: LetterCodeService

    @GetMapping(value = [""], produces = ["application/json"])
    fun getLetterCode(@Valid @ModelAttribute page: PaginationRequest): ResponseEntity<ReturnData> {
        return service.letterCode(page)
    }

    @GetMapping(value = ["work-unit"], produces = ["application/json"])
    fun workUnit(@Valid @ModelAttribute page: PaginationRequest): ResponseEntity<ReturnData> {
        return service.workUnit(page)
    }

    @GetMapping(value = ["doc-properties"], produces = ["application/json"])
    fun docProperties(@Valid @ModelAttribute page: PaginationRequest): ResponseEntity<ReturnData> {
        return service.docProperties(page)
    }
}
