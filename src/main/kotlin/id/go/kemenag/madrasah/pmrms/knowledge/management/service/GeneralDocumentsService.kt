package id.go.kemenag.madrasah.pmrms.knowledge.management.service

import id.go.kemenag.madrasah.pmrms.knowledge.management.constant.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.helpers.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.DocumentClassificationRequest
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.knowledge.management.model.response.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.*
import id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native.GeneralDocumentRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST")
@Service
class GeneralDocumentsService {

    @Autowired
    private lateinit var repo: GeneralDocumentsRepository

    @Autowired
    private lateinit var repoNative: GeneralDocumentRepositoryNative

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoGeneralDocumentsTag: GeneralDocumentsTagRepository

    @Autowired
    private lateinit var repoGeneralDocumentsTags: GeneralDocumentsTagsRepository

    @Autowired
    private lateinit var repoGeneralDocumentsVersion: GeneralDocumentsVersionRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Value("\${files.url}")
    private lateinit var filesUrl: String

    fun saveData(request: DocumentClassificationRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validate(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }
            val files: Files = validate["files"] as Files
            val createdNewVersion: GeneralDocumentsVersion? = validate["createdNewVersion"] as GeneralDocumentsVersion?

            if (createdNewVersion == null) {
                val data = GeneralDocuments()
                data.name = request.name
                data.filesId = request.filesId
                data.componentId = request.componentId
                data.files = files
                data.version = request.version
                data.description = request.description
                data.createdBy = getUserLogin()?.id

                if (files.attachmentTypeId == ATTACHMENT_TYPE_ID_MOM) {
                    data.meetingDate = validate["meetingDate"] as Date
                }

                if (request.componentId.isNullOrEmpty()) {
                    data.componentId = COMPONENT_EMPTY_ID
                }

                repo.save(data)

                val generalDocumentsTags = validate["generalDocumentsTags"] as List<GeneralDocumentsTag>?

                generalDocumentsTags?.forEach {
                    data.tags.add(
                        repoGeneralDocumentsTags.save(
                            GeneralDocumentsTags(
                                generalDocumentsId = data.id,
                                generalDocumentsTagId = it.id,
                                generalDocumentsTag = it
                            )
                        )
                    )
                }

                return responseCreated(data = data)
            } else {
                return responseCreated(data = createdNewVersion)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                repoNative.setParamInCondition("AND")
                if (getUserLogin()?.component?.code != "4.4") {
                    var componentId: String? = getUserLogin()?.component?.id
                    if (getUserLogin()?.component?.code?.contains("4.") == true) {
                        val findComponent = repoComponent.findByCodeAndActive("4")
                        if (findComponent.isPresent) {
                            componentId = findComponent.get().id
                        }
                    }
                    if (componentId != null) {
                        req.paramIn?.add(ParamArray("componentId", "string", listOf(componentId, COMPONENT_EMPTY_ID)))
                    } else {
                        req.paramIs?.add(
                            ParamSearch(
                                "componentId",
                                "string",
                                System.currentTimeMillis().toString()
                            )
                        )
                    }
                }
            }

            val filterTag: ParamArray? = req.paramIn?.find { ft ->
                ft.field == "tagId"
            }

            if (filterTag != null) {
                val generalDocumentsIds = repoGeneralDocumentsTags.getDocumentIdsByTags(filterTag.value as List<String>)
                if (generalDocumentsIds.isNotEmpty()) {
                    req.paramIn?.add(ParamArray("id", "string", generalDocumentsIds))
                } else {
                    req.paramIn?.add(ParamArray("id", "string", listOf(System.currentTimeMillis().toString())))
                }

                req.paramIn?.remove(filterTag)
            }

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: DocumentClassificationRequest): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.filesId.isNullOrEmpty()) {
                val chekFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.filesId ?: "", filesUrl, httpServletRequest.getHeader(
                        HEADER_STRING
                    )
                )
                val errorMessageFile = "Gagal melakukan request file"
                if (chekFile == null) {
                    listMessage.add(ErrorMessage("filesId", errorMessageFile))
                } else {
                    if (chekFile.success != true) {
                        listMessage.add(ErrorMessage("filesId", errorMessageFile))
                    } else {
                        if (chekFile.data == null) {
                            listMessage.add(ErrorMessage("filesId", errorMessageFile))
                        } else {
                            if (chekFile.data?.attachmentTypeId == ATTACHMENT_TYPE_ID_MOM) {
                                if (request.meetingDate.isNullOrEmpty()) {
                                    listMessage.add(
                                        ErrorMessage(
                                            "meetingDate",
                                            "Tanggal Rapat $VALIDATOR_MSG_REQUIRED"
                                        )
                                    )
                                } else {
                                    var meetingDate: Date? = null
                                    val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

                                    try {
                                        meetingDate = dform.parse(request.meetingDate)
                                    } catch (_: Exception) {
                                        listMessage.add(
                                            ErrorMessage(
                                                "meetingDate",
                                                "Tanggal Rapat $VALIDATOR_MSG_NOT_VALID"
                                            )
                                        )
                                    }

                                    if (meetingDate != null) {
                                        rData["meetingDate"] = meetingDate
                                    }
                                }
                            }


                            rData["files"] = chekFile.data!!
                        }
                    }
                }
            }

            var componentId = request.componentId
            if (componentId.isNullOrEmpty()) {
                componentId = COMPONENT_EMPTY_ID
            }

            var createdNewVersion: GeneralDocumentsVersion? = null

            val checkComponent = repoComponent.findByIdAndActive(componentId)
            if (!checkComponent.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "componentId",
                        "Unit Kerja ${componentId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                val check = repo.findByNameComponent(
                    request.name,
                    componentId
                )

                if (check.isPresent) {
                    if (check.get().version?.trim()?.lowercase() == request.version?.trim()?.lowercase()) {
                        listMessage.add(
                            ErrorMessage(
                                "filesId",
                                "Nama File ${request.name}, Versi ${request.version}, Unit Kerja ${checkComponent.get().code} $VALIDATOR_MSG_HAS_ADDED"
                            )
                        )
                    } else {
                        val checkVersion =
                            repoGeneralDocumentsVersion.findByGeneralDocumentsIdVersion(check.get().id, request.version)

                        if (checkVersion.isPresent) {
                            listMessage.add(
                                ErrorMessage(
                                    "filesId",
                                    "Nama File ${request.name}, Versi ${request.version}, Unit Kerja ${checkComponent.get().code} $VALIDATOR_MSG_HAS_ADDED"
                                )
                            )
                        } else {
                            createdNewVersion = repoGeneralDocumentsVersion.save(
                                GeneralDocumentsVersion(
                                    generalDocumentsId = check.get().id,
                                    filesId = request.filesId,
                                    version = request.version?.trim(),
                                    createdBy = getUserLogin()?.id
                                )
                            )
                        }
                    }
                }
            }

            val generalDocumentsTags = mutableListOf<GeneralDocumentsTag>()
            request.tagIds?.forEach {
                val checkTagById = repoGeneralDocumentsTag.findByIdAndActive(it)
                if (!checkTagById.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "tagIds",
                            "Tag Id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    generalDocumentsTags.add(checkTagById.get())
                }
            }

            request.newTags?.forEach {
                val checkNewTag = repoGeneralDocumentsTag.findByNameActive(it)
                if (checkNewTag.isPresent) {
                    generalDocumentsTags.add(checkNewTag.get())
                } else {
                    generalDocumentsTags.add(repoGeneralDocumentsTag.save(GeneralDocumentsTag(name = it.trim())))
                }
            }

            rData["generalDocumentsTags"] = generalDocumentsTags
            rData["createdNewVersion"] = createdNewVersion

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

}
