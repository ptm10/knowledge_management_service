package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.users.Component
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface ComponentRepository : JpaRepository<Component, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<Component>

    fun findByCodeAndActive(@Param("code") id: String?, active: Boolean = true): Optional<Component>
}
