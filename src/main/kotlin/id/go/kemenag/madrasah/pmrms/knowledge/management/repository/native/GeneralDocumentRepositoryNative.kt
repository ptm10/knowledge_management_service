package id.go.kemenag.madrasah.pmrms.knowledge.management.repository.native

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.GeneralDocuments
import org.springframework.stereotype.Repository

@Repository
class GeneralDocumentRepositoryNative : BaseRepositoryNative<GeneralDocuments>(GeneralDocuments::class.java)
