package id.go.kemenag.madrasah.pmrms.knowledge.management.repository

import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.LetterCode
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface LetterCodeRepository : JpaRepository<LetterCode, String> {
    @Query("FROM LetterCode WHERE LOWER(TRIM(:name)) = LOWER(TRIM(name)) AND active = :active")
    fun findByNameActive(
        @Param("name") name: String?,
        @Param("active") active: Boolean? = true
    ): Optional<LetterCode>

    fun findByIdAndActive(id: String? = null, active: Boolean? = true): Optional<LetterCode>

    @Query(
        "select r from LetterCode r " +
                "WHERE r.active = true AND (LOWER(r.name) LIKE %:param%)"
    )
    fun getListPagination(
        @Param("param") param: String?, pageable: Pageable?): Page<LetterCode?>?
}
