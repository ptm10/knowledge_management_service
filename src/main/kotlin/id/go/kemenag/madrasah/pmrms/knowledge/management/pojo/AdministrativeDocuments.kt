package id.go.kemenag.madrasah.pmrms.knowledge.management.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.users.Component
import id.go.kemenag.madrasah.pmrms.knowledge.management.pojo.users.Users
import java.util.*
import javax.persistence.*
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction

@Entity
@Table(name = "administrative_documents", schema = "public")
data class AdministrativeDocuments(
                @Id @Column(name = "id") var id: String? = UUID.randomUUID().toString(),
                @Column(name = "document_properties") var documentProperties: String? = null,
                @Column(name = "work_unit_id") var workUnitId: String? = null,
                @Column(name = "job_code") var jobCode: String? = null,
                @NotFound(action = NotFoundAction.IGNORE)
                @ManyToOne
                @JoinColumn(
                                name = "component_id",
                                insertable = false,
                                updatable = false,
                                nullable = true
                )
                var component: Component? = null,
                @Column(name = "component_id") var componentId: String? = null,
                @Column(name = "letter_code") var letterCode: String? = null,
                @Column(name = "letter_number_date")
                @get:JsonFormat(
                                shape = JsonFormat.Shape.STRING,
                                pattern = "dd-MM-yyyy HH:mm:ss",
                                timezone = "GMT+7"
                )
                var letterNumberDate: Date? = Date(),
                @Column(name = "letter_number") var letterNumber: String? = null,
                @Column(name = "description") var description: String? = null,
                @Column(name = "status") var status: String? = "Digunakan",
                @Column(name = "note") var note: String? = null,
                @Column(name = "created_at")
                @get:JsonFormat(
                                shape = JsonFormat.Shape.STRING,
                                pattern = "dd-MM-yyyy HH:mm:ss",
                                timezone = "GMT+7"
                )
                var createdAt: Date? = Date(),
                @Column(name = "created_by") var createdBy: String? = null,
                @ManyToOne
                @JoinColumn(
                                name = "created_by",
                                insertable = false,
                                updatable = false,
                                nullable = true
                )
                var createdByUsers: Users? = null,
                @Column(name = "updated_at")
                @get:JsonFormat(
                                shape = JsonFormat.Shape.STRING,
                                pattern = "dd-MM-yyyy HH:mm:ss",
                                timezone = "GMT+7"
                )
                var updatedAt: Date? = Date(),
                @Column(name = "updated_by") var updatedBy: String? = null,
                @Column(name = "active") @JsonIgnore var active: Boolean? = true,
)

data class AdministrativeDocumentsResponse(
                var rowNumber: Number = 0,
                var id: String? = "",
                var documentProperties: String? = null,
                var workUnitId: String? = null,
                var jobCode: String? = null,
                var component: Component? = null,
                var componentId: String? = null,
                var letterCode: String? = null,
                var letterNumberDate: Date? = null,
                var letterNumber: String? = null,
                var description: String? = null,
                var status: String? = "",
                var note: String? = null,
                var createdAt: Date? = null,
                var createdBy: String? = null,
                var createdByUsers: Users? = null,
                var updatedAt: Date? = null,
                var active: Boolean? = true
)
