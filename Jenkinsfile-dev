pipeline {
    agent any 
    tools {
        maven 'Maven 3.8.4'
    }
	
    environment {
        PROJECT_ID = 'ps-id-cerindocorp-14012022'
        CLUSTER_NAME = 'pmrms-dev'
        LOCATION = 'asia-southeast2'
        CREDENTIALS_ID = 'gke-private-key'
    }
    
    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        stage('Build image') {
            steps {
                script {
					sh "cp src/main/resources/application-dev.properties src/main/resources/application.properties"
					sh "mvn clean package"
                    app = docker.build("mailpaps/pmrms-knowledge-management-dev:${env.BUILD_ID}")
                }
            }
        }
        
        stage('Push image') {
            steps {
                script {
                    withCredentials( \
                                 [string(credentialsId: 'dockerhub',\
                                 variable: 'dockerhub')]) {
                        sh "docker login -u mailpaps -p ${dockerhub}"
                    }
                    app.push("${env.BUILD_ID}")
                 }                                 
            }
        }
    
        stage('Deploy to K8s') {
            steps{
                echo "Deployment started ..."
                sh "sed -i 's/pmrms-knowledge-management-dev:latest/pmrms-knowledge-management-dev:${env.BUILD_ID}/g' deployment-dev.yaml"
                step([$class: 'KubernetesEngineBuilder', \
                  projectId: env.PROJECT_ID, \
                  clusterName: env.CLUSTER_NAME, \
                  location: env.LOCATION, \
                  manifestPattern: 'deployment-dev.yaml', \
                  credentialsId: env.CREDENTIALS_ID, \
                  verifyDeployments: true])
                }
            }
        }    
}
