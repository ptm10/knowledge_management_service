FROM openjdk:11.0.13-jdk-slim

WORKDIR /opt/app

RUN apt update -y && apt install -y maven

COPY . .

COPY settings.xml /root/.m2/settings.xml

ARG BUILD_ENV=production

RUN cp src/main/resources/application-${BUILD_ENV}.properties src/main/resources/application.properties
RUN mvn package -s settings.xml -Dmaven.test.skip=true 

RUN mkdir -p /usr/local/pmrms
RUN cp target/pmrms-knowledge-management-1.0.0.jar /usr/local/pmrms/pmrms-knowledge-management.jar
WORKDIR /usr/local/pmrms
CMD ["java", "-jar", "pmrms-knowledge-management.jar"] 
